<?php
  switch (true) {
    case preg_match ("/https?\:\/\/(www\.)?sorozat\-?barat\.[^\/]+\/rss\/episodes/i", $feed["url"]):
      $body = preg_replace_callback (
        "/(<link>)([^\<\)]+?)(<\/link>)/imU"
        , function ($reg) { return $reg[1] . str_replace(" ","+",$reg[2]) . $reg[3]; }
        , $body);
      $body = preg_replace ("/(<link>)([^\<]+?)(<\/link>)/imU", "\\1\\2\\3<guid>\\2</guid>", $body);
      break;
  }
