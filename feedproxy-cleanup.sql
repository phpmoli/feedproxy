CREATE TEMPORARY TABLE states_all
  SELECT entryid, state
  FROM feedproxy_subs
  INNER JOIN feedproxy_entry USING (feedid)
  LEFT JOIN feedproxy_state USING (entryid, account)
  WHERE
    active='yes'
    AND content IS NOT NULL
    AND content!='';

CREATE TEMPORARY TABLE states_min
  SELECT entryid, MIN(IFNULL(state,'')) AS min
  FROM states_all
  GROUP BY entryid;

UPDATE feedproxy_entry
SET summary=NULL, content=NULL
WHERE
  entryid IN (
    SELECT entryid
    FROM states_min
    WHERE
      min IS NOT NULL
      AND min!='');

OPTIMIZE TABLE feedproxy_entry;
