<?php
  function error ($no, $text, $file, $line) {
    restore_error_handler ();
    error_log (date ("Y-m-d H:i:s") . "\tphp #$no $text @$file #$line\n", 3, (!empty ($GLOBALS["BASE"]) ? $GLOBALS["BASE"] : "feedproxy") . ".log");
    exit (1);
  }
  set_error_handler ("error");

  error_reporting (E_ALL);


  $BASE = strtok (basename ($_SERVER["SCRIPT_NAME"], ".php"), "-");

  header ("hu-moli-$BASE-launch: true", true);


  if (is_readable ("$BASE.conf")) $conf = parse_ini_file ("$BASE.conf", true);
  if (!empty ($conf["database"])) $DB = $conf["database"];
  if (!empty ($conf["reader"])) {
    foreach ($conf["reader"] as $key => $dummy) {
      if (isset ($_GET[$key])) $conf["reader"][$key] = $_GET[$key];
    }
  }

  if ($dir = opendir (".")) {
    while (false !== ($filename=readdir($dir))) {
      if (preg_match ("/^pw\_(?:account\_)?(.+)\_[0-9a-f]{32}\.php$/i", $filename, $reg)
        && ($content=file($filename))
        && !empty($content[2])
        && ($pw=trim($content[2]))
        && !empty($pw)) {
        if ($reg[1] == "db") $DB["password"] = $pw;
        else $auths[$reg[1]] = $pw;
      }
    }
    closedir ($dir);
  }


  function exception ($e) {
    error_log (
      date ("Y-m-d H:i:s") . "\texception #" . $e->getCode () . " " . $e->getMessage () . " @" . $e->getFile () . "#" . $e->getLine ()
      . "\t" . preg_replace("/[\r\n\t\s ]+/"," ",$e->getTraceAsString()) . "\n"
      , 3, (!empty ($GLOBALS["BASE"]) ? $GLOBALS["BASE"] : "feedproxy") . ".log");
    header ("status: 500 Internal Server Error", true, 500);
    exit (255);
  }
  set_exception_handler ("exception");

  $DB["link"] = new PDO (
    "mysql:" . (!empty($DB["socket"]) ? "unix_socket={$DB["socket"]}" : (!empty($DB["hostname"]) ? "host={$DB["hostname"]}" : "host=localhost"))
    . ";dbname=" . (!empty($DB["database"]) ? $DB["database"] : "test")
    . ";charset=UTF8"
    , (!empty($DB["username"]) ? $DB["username"] : "root")
    , (!empty ($DB["password"]) ? $DB["password"] : null)
    , array (PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));


  $account = (!empty ($_GET["account"]) ? $_GET["account"] : "");


  header ("pragma: no-cache", true);
  header ("cache-control: private, no-cache, no-store, no-transform, must-revalidate, max-age=0, s-maxage=0, no-archive", true);
  header ("last-modified: " . gmdate (DATE_RFC1123), true);
  header ("expires: Thu, 31 Dec 1970 00:00:00 GMT", true);


  if (!empty ($_GET["read"])) {
    if (!empty ($_GET["auth"])) {
      $qry = "UPDATE {$BASE}_state
               SET
                 state='read'
                 , ip=:ip
                 , touch=NOW()
               WHERE
                 entryid=:entryid
                 AND account=:account
                 AND auth=:auth";
      $st = $DB["link"]->prepare ($qry);
      $st->execute (array(
        ":ip" => $_SERVER["REMOTE_ADDR"]
        , ":entryid" => $_GET["read"]
        , ":account" => $account
        , ":auth" => $_GET["auth"]));
      if (!$st->rowCount()) header ("status: 500 Internal Server Error", true, 500);
      $st->closeCursor ();
    }

    header ("content-type: image/png", true);
    echo base64_decode ("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=");
/*
    header ("content-type: image/svg+xml", true);
    echo '<?xml version="1.0"?><svg xmlns="http://www.w3.org/2000/svg" width="1px" height="1px"></svg>';
*/


  } elseif (!empty ($_GET["visit"])) {
    $qry = "SELECT link
            FROM {$BASE}_state
            INNER JOIN {$BASE}_entry AS entry USING (entryid)
            WHERE
              entry.entryid=:entryid
              AND account=:account
              AND auth" . (!empty ($_GET["auth"]) ? "=:auth" : " IS NULL") . "
            LIMIT 1";
    $st = $DB["link"]->prepare ($qry);
    $st->bindValue (":entryid", $_GET["visit"], PDO::PARAM_STR);
    $st->bindValue (":account", $account, PDO::PARAM_STR);
    if (!empty ($_GET["auth"])) $st->bindValue (":auth", $_GET["auth"], PDO::PARAM_STR);
    $st->execute ();

    if (!$st->rowCount()) {
      header ("status: 403 Forbidden", true, 403);
    } else {
      $elink = $st->fetchColumn ();
      $st->closeCursor ();

      $qry = "UPDATE {$BASE}_state
               SET
                 state='visited'
                 , ip=:ip
                 , touch=NOW()
               WHERE
                 entryid=:entryid
                 AND account=:account
                 AND auth" . (!empty ($_GET["auth"]) ? "=:auth" : " IS NULL");
      $st = $DB["link"]->prepare ($qry);
      $bind = array(
        ":ip" => $_SERVER["REMOTE_ADDR"]
        , ":entryid" => $_GET["visit"]
        , ":account" => $account);
      if (!empty ($_GET["auth"])) $bind[":auth"] = $_GET["auth"];
      $st->execute ($bind);
      $st->closeCursor ();

      header ("status: 302 Found", true, 302);
      header ("location: "
        . (empty ($elink)
          ? "{$conf["base"]["url"]}feedproxy.php?account=" . rawurlencode ($account) . "&text={$_GET["visit"]}"
            . (!empty ($_GET["auth"]) ? "&auth=" . rawurlencode ($_GET["auth"]) : "")
          : $elink)
        , true);
    }


  } elseif (!empty ($_GET["text"])) {
    $qry = "SELECT content
            FROM {$BASE}_state AS state INNER JOIN {$BASE}_entry USING (entryid)
            WHERE
              state.entryid=:entryid
              AND account=:account
              AND auth" . (!empty ($_GET["auth"]) ? "=:auth" : " IS NULL") . "
            LIMIT 1";
    $st = $DB["link"]->prepare ($qry);
    $st->bindValue (":entryid", $_GET["text"], PDO::PARAM_STR);
    $st->bindValue (":account", $account, PDO::PARAM_STR);
    if (!empty ($_GET["auth"])) $st->bindValue (":auth", $_GET["auth"], PDO::PARAM_STR);
    $st->execute ();

    if (!$st->rowCount()) {
      header ("status: 403 Forbidden", true, 403);
    } else {
      $econtent = $st->fetchColumn ();
      $st->closeCursor ();
      echo $econtent;
    }


  } else {
    if (!empty ($auths[$account]) && (empty ($_GET["auth"]) || $auths[$account] != $_GET["auth"])) {
      header ("status: 403 Forbidden", true, 403);


    } else {
      $qry = "SELECT
                url, feed.title, feed.link
                , entry.entryid, entry.groupid, UNIX_TIMESTAMP(published), UNIX_TIMESTAMP(entry.updated)
                , uniqueid, entry.link, author, author_url, author_email, entry.title, summary, content
                , forwardedto, auth
              FROM {$BASE}_subs AS subs INNER JOIN {$BASE}_feed AS feed USING (feedid)
              INNER JOIN {$BASE}_entry AS entry USING (feedid)
              LEFT JOIN {$BASE}_state AS state USING (entryid, account)
              WHERE
                subs.account=:account
                AND subs.active IS NOT NULL
                AND (state IS NULL OR state='new')"
        . (empty ($conf["reader"]["is_broken"]) ? "" :
              " AND (
                  forwardedto IS NULL"
          . (empty ($_GET["host"]) ? "" :
                " OR forwardedto=:forwardedto")
            .  ")
              ORDER BY forwardedto ASC, forwardedtimes DESC, entry.updated ASC, published ASC, created ASC, entry.entryid ASC")
        .   " LIMIT 500";
      $st = $DB["link"]->prepare ($qry);
      $st->bindValue (":account", $account, PDO::PARAM_STR);
      if (!empty ($conf["reader"]["is_broken"]) && !empty ($_GET["host"])) $st->bindValue (":forwardedto", $_GET["host"], PDO::PARAM_STR);
      $st->execute ();

      if (!$st->rowCount() && empty ($conf["reader"]["missing_http204"])) {
        header ("status: 204 No Content", true, 204);
        header ("content-type: application/atom+xml", true);
        header ("content-length: 0", true);


      } else {
        header ("content-type: application/atom+xml", true);

        $out = <<<HEREDOC
<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="en">

HEREDOC;
        $out .= "  <id>http://$BASE/" . rawurlencode ($account) . "</id>\n";
        $out .= "  <updated>" . date ("c") . "</updated>\n";
        $out .= "  <title>$BASE" . ($account ? htmlspecialchars (" ".$account, ENT_QUOTES, "UTF-8") : "") . "</title>\n";
        $out .= "  <author><name></name></author>\n";

        if (!empty ($conf["reader"]["is_broken"])) $new = 0;
        $forwarded = array ();
        while (
          list (
            $furl, $ftitle, $flink
            , $eid, $egroupid, $epublished, $eupdated, $euniqueid, $elink, $eauthor, $eauthor_url, $eauthor_email, $etitle, $esummary, $econtent
            , $forwardedto, $auth) = $st->fetch (PDO::FETCH_NUM)) {
              $euniqueid = preg_replace("/^https\:\/\//", "http://", $euniqueid);

              if (!empty ($conf["reader"]["is_broken"])) {
                if (!$forwardedto) $new++;
                elseif ($new) break;
              }
              if (empty ($auth)) $auth = substr (preg_replace ("/[^a-z0-9]/i", "", base64_encode (sha1 (rand ()))), 0, 8);
              if (!empty ($conf["reader"]["missing_author"])) {
                $etitle = $ftitle . " / " . $etitle;
              }


              $out .= "  <entry>\n";
              $out .= "    <id>" . "http://$BASE/" . rawurlencode ($account) . "?" . rawurlencode ($egroupid) . "#" . rawurlencode ($euniqueid)
                . (!empty ($conf["reader"]["missing_updated"]) ? "__" . $eupdated : "") . "</id>\n";
              if ($epublished) $out .= "    <published>" . date("c", $epublished) . "</published>\n";
              if ($eupdated) $out .= "    <updated>" . date ("c", $eupdated) . "</updated>\n";
              $out .= "    <link rel=\"alternate\" href=\""
                . htmlspecialchars (
                  (!empty ($conf["reader"]["state_visited"])
                    ? "{$conf["base"]["url"]}feedproxy.php?account=" . rawurlencode ($account) . "&visit=$eid&auth=" . rawurlencode ($auth)
                    : (empty ($elink)
                      ? "{$conf["base"]["url"]}feedproxy.php?account=" . rawurlencode ($account) . "&text=$eid&auth=" . rawurlencode ($auth)
                      : $elink))
                  , ENT_QUOTES, "UTF-8")
                . "\"/>\n";
              $out .= "    <author>\n";
              $out .= "      <name>"
                . htmlspecialchars ($ftitle . ($eauthor && !preg_match ("/\/feedph\//", $furl) ? " - ".$eauthor : ""), ENT_QUOTES, "UTF-8")
                . "</name>\n";
              $out .= "      <uri>" . htmlspecialchars (($eauthor_url ? $eauthor_url : $flink), ENT_QUOTES, "UTF-8") . "</uri>\n";
              if (!empty ($eauthor_email)) $out .= "      <email>" . htmlspecialchars ($eauthor_email, ENT_QUOTES, "UTF-8") . "</email>\n";
              $out .= "    </author>\n";
              $out .= "    <title type=\"html\">"
                . htmlspecialchars ($etitle . (preg_match ("/\/feedph\//", $furl) && $eauthor ? " - ".$eauthor : ""), ENT_QUOTES, "UTF-8")
                . "</title>\n";
              if ($esummary) $out .= "    <summary type=\"html\">" . htmlspecialchars ($esummary, ENT_QUOTES, "UTF-8") . "</summary>\n";
              $out .= "    <content type=\"html\">"
                . htmlspecialchars (
                  (!empty ($conf["reader"]["state_read"])
                    ? "<img src=\"{$conf["base"]["url"]}feedproxy.php?account=" . rawurlencode ($account)
                      . "&read=$eid&auth=" . rawurlencode ($auth) . "\"/>" : "")
                  . ($econtent ? $econtent : $elink)
                  , ENT_QUOTES | ENT_IGNORE, "UTF-8")
                . "</content>\n";
              $out .= "  </entry>\n";


              $forwarded[$eid] = array ("isforwarded" => $forwardedto, "auth" => $auth);

              if (!empty ($conf["reader"]["is_broken"]) && $forwardedto) break;
        }
        $st->closeCursor ();
        $out .= "</feed>\n";


        if (!empty ($forwarded)) {
          $qryi = "INSERT INTO {$BASE}_state
                   SET
                     entryid=:entryid
                     , account=:account
                     , ip=:ip
                     , touch=NOW()"
            . (empty ($conf["reader"]["is_broken"]) ? "" :
                    ", forwardedtimes=1")
            . (empty ($_GET["host"]) ? "" :
                    ", forwardedto=:forwardedto")
            .       ", auth=:auth
                   ON DUPLICATE KEY UPDATE
                     touch=NOW()
                     , forwardedtimes=forwardedtimes+1";
          $sti = $DB["link"]->prepare ($qryi);
          $sti->bindParam (":entryid", $eid, PDO::PARAM_STR);
          $sti->bindParam (":account", $account, PDO::PARAM_STR);
          $sti->bindParam (":ip", $_SERVER["REMOTE_ADDR"], PDO::PARAM_STR);
          $sti->bindParam (":forwardedto", $_GET["host"], PDO::PARAM_STR);

          $qryif = "INSERT INTO {$BASE}_state
                    SET
                      entryid=:entryid
                      , account=:account
                      , ip=:ip
                      , touch=NOW()
                      , state='forwarded'"
            . (empty ($conf["reader"]["is_broken"]) ? "" :
                     ", forwardedtimes=1")
            . (empty ($_GET["host"]) ? "" :
                     ", forwardedto=:forwardedto")
            .        ", auth=:auth
                    ON DUPLICATE KEY UPDATE
                      touch=NOW()
                      , forwardedtimes=forwardedtimes+1
                      , state='forwarded'";
          $stif = $DB["link"]->prepare ($qryif);
          $stif->bindParam (":entryid", $eid, PDO::PARAM_STR);
          $stif->bindParam (":account", $account, PDO::PARAM_STR);
          $stif->bindParam (":ip", $_SERVER["REMOTE_ADDR"], PDO::PARAM_STR);
          $stif->bindParam (":forwardedto", $_GET["host"], PDO::PARAM_STR);

          foreach ($forwarded as $eid => $arr) {
            if (empty ($conf["reader"]["is_broken"]) || $arr["isforwarded"] || $new==1) {
              $stif->bindValue (":auth", $arr["auth"], PDO::PARAM_STR);
              $stif->execute ();
            } else {
              $sti->bindValue (":auth", $arr["auth"], PDO::PARAM_STR);
              $sti->execute ();
            }
          }
          $sti->closeCursor ();
          $stif->closeCursor ();
        }


        echo $out;
      }
    }
  }
