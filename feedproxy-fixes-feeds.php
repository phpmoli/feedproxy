<?php
  switch (true) {
    case strpos($feed["url"],"444.hu/feed"):
      $feed["link"] = preg_replace("/\/feed\/?$/","/",$feed["link"]);
      break;
    case strpos($feed["url"],"apkmirror.com/apk/"):
      $feed["link"] = preg_replace("/\/feed\/?$/","/",$feed["url"]);
      break;
    case strpos($feed["url"],".blog.hu/atom"):
      if (preg_match("/<link rel=\"self\" type=\"application\/atom\+xml\" href=\"([^\"\>]+?)\"\/>/m", $body, $reg)) $feed["link"] = $reg[1];
      break;
    case strpos ($feed["url"], "campaign-archive.com/feed?"):
      $feed["link"] = str_replace("campaign-archive.com/feed","campaign-archive.com/home",$feed["url"]);
      break;
    case preg_match ("/^https?:\/\/(www\.)?index\.hu\//", $feed["url"]):
    case strpos($feed["url"],"/feedindex/-"):
      $feed["link"] = preg_replace("/\/\/$/","/",$feed["link"]);
      break;
    case strpos($feed["url"],"//retro.land/rss"):
      $feed["link"] = preg_replace("/\/rss\/?$/","/",$feed["link"]);
      break;
    case strpos($feed["url"],"//rtl.hu/"):
      $feed["link"] = preg_replace("/^(?:(?:https?)?:)?(?:\/\/){1,2}/","https://",$feed["link"]);
      break;
    case preg_match ("/\<atom:link\s+href\=\"/im", $body):
    case strpos($feed["url"],"/feedrarbg"):
      $feed["link"] = $fobj->getLink (0, "href");
      break;
  }

  switch (true) {
    case strpos($feed["url"],"apkmirror.com/apk/"):
      $feed["title"] = "APKMirror";
      break;
    case strpos($feed["url"],"chromeunboxed.com/feed"):
      $feed["title"] = "Chrome Unboxed";
      break;
    case strpos($feed["url"],"debian.org/News/news"):
      $feed["title"] = "Debian";
      break;
    case strpos ($feed["url"], "campaign-archive.com/feed?"):
      $feed["title"] = str_replace(" Archive Feed","",$feed["title"]);
      break;
    case strpos($feed["url"],"//feeds.feedburner.com/GoogleChromeReleases"):
      $feed["title"] = "ChromeOS";
      break;
    case strpos($feed["url"],"//feeds.feedburner.com/GoogleOnlineSecurityBlog"):
      $feed["title"] = "Google Security";
      break;
    case strpos($feed["url"],"hwsw.hu/xml/latest_news_rss.xml"):
      $feed["title"] = "HWSW";
      break;
    case strpos($feed["url"],"/tudomany/til/rss"):
      $feed["title"] = "Index - TIL";
      break;
    case preg_match ("/^https?:\/\/(www\.)?index\.hu\//", $feed["url"]):
    case strpos($feed["url"],"/feedindex/"):
      $feed["title"] = preg_replace("/ cikkek$/", "", $feed["title"]);
      break;
    case strpos($feed["url"],"//retro.land/rss"):
      $feed["title"] = "Retroland";
      break;
    case preg_match ("/https?:\/\/(www\.)rtl\.hu\//", $feed["url"]):
      $feed["title"] = preg_replace("/^Vide.+?k \| /", "", $feed["title"]);
      break;
  }
