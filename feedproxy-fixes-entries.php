<?php
  switch (true) {
    case preg_match ("/^https?:\/\/(?:www\.)?apkmirror\.com\/apk\/[^\/]+\/feed\/?$/", $feed["url"]):
    case strpos($feed["url"],"blog.google/products/"):
    case strpos($feed["url"],"feeds.feedburner.com/GoogleOnlineSecurityBlog"):
      $entry["updated"] = 1;
      break;
  }

  switch (true) {
    case preg_match ("/^https?:\/\/(?:www\.)?apkmirror\.com\/apk\/[^\/]+\/feed\/?$/", $feed["url"]):
      $entry["uniqueid"] = preg_replace ("/^(https?:\/\/(?:www\.)?apkmirror\.com\/apk\/[^\/]+\/[^\/]+\/)[^\/]+\/?$/", "\\1", $entry["link"]);
      break;
    case strpos($feed["url"],"blog.google/products/"):
    case strpos($feed["url"],"feeds.feedburner.com/GoogleOnlineSecurityBlog"):
      $entry["uniqueid"] = preg_replace (
        "/[^a-z0-9]/"
        , "_"
        , strtolower (
          (!empty($entry["title"])
          ? $entry["title"]
          : preg_replace("/^.+\/([^\/]+)\.html$/i", "\\1", (!empty($entry["link"]) ? $entry["link"] : $entry["uniqueid"])))));
      break;
    case preg_match ("/^https?\:\/\/(?:www\.)?sorozat\-?barat\./", $feed["url"]):
      $entry["uniqueid"] = preg_replace("/^(http\:\/\/)www\.sorozat\-?barat\..+?(\/video\/.+)$/i", "\\1sorozatbarat\\2", $entry["uniqueid"])
        . "?{$entry["updated"]}";
      break;
  }

  switch (true) {
    case preg_match ("/^https?\:\/\/feeds[^\.]*\.feedburner\.com\/(GoogleOnlineSecurityBlog|GoogleChromeReleases)/", $feed["url"]):
      $i = 0;
      while ($link = $eobj->getLink ($i, "href")) {
        if (substr ($link, -5) === ".html") {
          $entry["link"] = $link;
          break;
        }
        $i++;
      }
      break;
    case preg_match ("/https?:\/\/(www\.)rtl\.hu\//", $feed["url"]):
      $entry["link"] = "http:" . $entry["link"];
      break;
  }
