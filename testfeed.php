<?php
  $conf = parse_ini_file ("feedproxy.conf");
  $filepath="{$conf["base"]["url"]}feedproxy/testfeed.php";
  $title="test";
  $now = date ("c");
  $stamp = time ();
  header ("content-type: application/atom+xml", true);
  echo <<<HEREDOC
<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="en">
  <id>$filepath</id>
  <updated>$now</updated>
  <title>$title</title>
  <link rel="self" href="$filepath" type="application/atom+xml" title="$title" hreflang="en"/>
  <author><name>$title</name></author>
  <entry>
    <id>$filepath?$stamp</id>
    <updated>$now</updated>
    <title>new $now</title>
    <content type="html">new $stamp</content>
  </entry>
  <entry>
    <id>$filepath</id>
    <updated>$now</updated>
    <title>updated $now</title>
    <content type="html">updated $now</content>
  </entry>
</feed>
HEREDOC;
?>