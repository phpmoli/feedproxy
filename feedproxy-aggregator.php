<?php
#  define ("debug", true);

  ini_set ("display_errors", true);
  error_reporting (E_ALL);


  if (!include_once ("XML/Feed/Parser.php")) trigger_error ("XML_Feed_Parser PEAR package not found", E_USER_ERROR);

  if (!extension_loaded ("SimpleXML")) trigger_error ("SimpleXML PHP extension not found", E_USER_ERROR);
  libxml_use_internal_errors (true);


  $BASE = strtok (basename ($_SERVER["SCRIPT_NAME"], ".php"), "-");

  if (is_readable ("$BASE.conf")) $conf = parse_ini_file ("$BASE.conf", true);
  if (!empty ($conf["database"])) $DB = $conf["database"];


  foreach(array("sources","feeds","entries") as $fix) {
    if (!file_exists ("$BASE-fixes-$fix.php") || !is_readable ("$BASE-fixes-$fix.php")) trigger_error ("fixes-$fix include not found", E_USER_ERROR);
  }


  if (!file_exists ("$BASE-skip.php")) trigger_error ("skip include not found", E_USER_ERROR);
  include_once ("$BASE-skip.php");
  if (!function_exists ("skip")) trigger_error ("skip function not found", E_USER_ERROR);


  if ($dir = opendir (".")) {
    while (false !== ($filename=readdir($dir))) {
      if (preg_match ("/^pw\_(.+)\_[0-9a-f]{32}\.php$/i", $filename, $reg)
        && ($content=file($filename))
        && !empty($content[2])
        && ($pw=trim($content[2]))
        && !empty($pw)) {
        if ($reg[1] == "db") $DB["password"] = $pw;
        else $PW[$reg[1]] = $pw;
      }
    }
    closedir ($dir);
  }


  function exception ($e) {
    error_log (
      date ("Y-m-d H:i:s") . "\texception #" . $e->getCode () . " " . $e->getMessage () . " @" . $e->getFile () . "#" . $e->getLine () . "\n"
      , 3, (!empty ($GLOBALS["BASE"]) ? $GLOBALS["BASE"] : "feedproxy") . ".log");
    exit (255);
  }
  set_exception_handler ("exception");

  $DB["link"] = new PDO (
    "mysql:" . (!empty($DB["socket"]) ? "unix_socket={$DB["socket"]}" : (!empty($DB["hostname"]) ? "host={$DB["hostname"]}" : "host=localhost"))
    . ";dbname=" . (!empty($DB["database"]) ? $DB["database"] : "test")
    . ";charset=UTF8"
    , (!empty($DB["username"]) ? $DB["username"] : "root")
    , (!empty ($DB["password"]) ? $DB["password"] : null)
    , array (PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));


  if (!($curl=curl_init ()) || curl_errno($curl)) trigger_error ("curl init #" . curl_errno ($curl) . " " . curl_error ($curl), E_USER_ERROR);
  curl_setopt_array ($curl, array (
      CURLOPT_USERAGENT => "feedproxy (+https://gitlab.com/phpmoli/feedproxy)",
      CURLOPT_ENCODING => "",
      CURLOPT_CONNECTTIMEOUT => 12,
      CURLOPT_TIMEOUT => 128,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_CRLF => true,
    ));


  $qry1 = "SELECT uniqueid, UNIX_TIMESTAMP(updated)
           FROM {$BASE}_entry
           WHERE
             groupid=:groupid";
  $st1 = $DB["link"]->prepare ($qry1);
  $groupid = null;
  $st1->bindParam (":groupid", $groupid, PDO::PARAM_STR);


  $qry2 = "SELECT @entryid:=entryid
           FROM {$BASE}_entry
           WHERE
             groupid=:groupid
             AND uniqueid=:uniqueid
           LIMIT 1";
  $st2 = $DB["link"]->prepare ($qry2);
  $groupid = null;
  $st2->bindParam (":groupid", $groupid, PDO::PARAM_STR);
  $uniqueid = null;
  $st2->bindParam (":uniqueid", $uniqueid, PDO::PARAM_STR);


  $qry3 = "DELETE FROM {$BASE}_state
           WHERE entryid=@entryid";
  $st3 = $DB["link"]->prepare ($qry3);


  $qry4 = "INSERT IGNORE INTO {$BASE}_state
           SET
             entryid=@entryid
             , account=:account
             , touch=NOW()
             , state='new'
             , forwardedto=NULL
             , forwardedtimes=0
             , auth=:auth";
  $st4 = $DB["link"]->prepare ($qry4);


  $qry5 = "UPDATE {$BASE}_state
           SET
             state='forwarded'"
    . (empty ($conf["reader"]["is_broken"]) ? "" : 
            ", forwardedto=:forwardedto")
    .       ", forwardedtimes=1
             , touch=NOW()
           WHERE
             entryid=@entryid
             AND account=:account
             AND auth=:auth";
  $st5 = $DB["link"]->prepare ($qry5);


  $qry0 = "SELECT
             feedid, IFNULL(groupid, feedid) AS groupid, url, etag, UNIX_TIMESTAMP(lastmod) AS lastmod, UNIX_TIMESTAMP(updated) AS updated
             , GROUP_CONCAT(account) AS accountstr
             , GROUP_CONCAT(IFNULL(kindleit, 'no')) AS kindleitstr
             , GROUP_CONCAT(IFNULL(emailto, '') SEPARATOR '|') AS emailtostr
           FROM {$BASE}_feed INNER JOIN {$BASE}_subs USING (feedid)
           WHERE
             nextfetch<DATE_ADD(NOW(), INTERVAL 1 MINUTE)
             AND url NOT LIKE 'about:%'
             AND active IS NOT NULL
           GROUP BY feedid
           ORDER BY RAND()";
  $st0 = $DB["link"]->query ($qry0);
  while ($feed = $st0->fetch (PDO::FETCH_ASSOC)) {
    $kindleits = array ();
    $emailtos = array ();
    $accountarr = explode (",", $feed["accountstr"]);
    $kindleitarr = explode (",", $feed["kindleitstr"]);
    $emailtoarr = explode ("|", $feed["emailtostr"]);
    foreach ($accountarr as $key => $account) {
      if ($kindleitarr[$key] == "yes") $kindleits[$account] = $kindleitarr[$key];
      if (!empty ($emailtoarr[$key])) $emailtos[$account] = $emailtoarr[$key];
    }

    set_time_limit (256);

    curl_setopt_array ($curl, array (
        CURLOPT_URL => $feed["url"],
        CURLOPT_FAILONERROR => true,
        CURLOPT_HEADER => true,
        CURLOPT_HTTPHEADER => array(),
        CURLOPT_POST => false,
      ));

    $headers = array ();
    if ($feed["etag"]) $headers[] = "if-none-match: " . $feed["etag"];
    if ($feed["lastmod"]) $headers[] = "if-modified-since: " . date("r", $feed["lastmod"]);
    if ($headers) curl_setopt ($curl, CURLOPT_HTTPHEADER, $headers);

    if (!($response=curl_exec($curl)) || curl_errno($curl)) {
      if (strpos($feed["url"],"/feedindex/") && curl_error($curl)=="The requested URL returned error: 400") {
        error_log (date ("Y-m-d H:i:s") . "\tcurl exec #" . curl_errno ($curl) . " " . curl_error ($curl) . " of '{$feed["url"]}'\n", 3, "$BASE.log");
      }
    } else {
      $pos = strpos ($response, "\r\n\r\n");
      $headers = ($pos ? substr ($response, 0, $pos + 2) : $response);
      $redirected = null;

      foreach (explode ("\r\n", $headers) as $line) {
        $header = strtolower (strtok ($line, ":"));
        $value = trim (strtok (""));
        if ($header == "etag") $feed["etag"] = $value;
        if ($header == "last-modified") $feed["lastmod"] = strtotime($value);
        if ($header == "location") $redirected = $value;
      }

      if (!empty ($redirected)) {
        error_log(date("Y-m-d H:i:s")."\tredirected '{$feed["url"]}' to '$redirected'\t".preg_replace("/[\r\n\t\s]+/"," ",$headers)."\n", 3, "$BASE.log");
      } elseif ($pos && ($body=substr($response,$pos+4)) && $body) {
        $groupid = $feed["groupid"];
        $st1->execute();
        $entries = $st1->fetchAll (PDO::FETCH_KEY_PAIR);
        $st1->closeCursor ();


        include ("$BASE-fixes-sources.php");


        try {
          $fobj = @new XML_Feed_Parser ($body, false, true, true);


          if (trim((string) $fobj->title)) $feed["title"] = trim((string) $fobj->title);
          if (trim((string) $fobj->link)) $feed["link"] = trim((string) $fobj->link);


          include ("$BASE-fixes-feeds.php");


          if (!trim((string) $fobj->updated) || trim((string) $fobj->updated) != $feed["updated"]) {
            if (defined("debug") && constant ("debug")) error_log (date ("Y-m-d") . "\t{$feed["url"]}\tupdated\n", 3, "debug/{$BASE}_debug.log");

            if (trim((string) $fobj->updated)) $feed["updated"] = trim((string) $fobj->updated);


            while ($fobj->valid ()) {
              $entry = array ();
              $eobj = $fobj->current ();


              if (trim((string) $eobj->published) && trim((string) $eobj->updated)) $entry["published"] = trim((string) $eobj->published);

              $entry["updated"] = (trim((string) $eobj->updated)
                ? trim((string) $eobj->updated)
                : (trim((string) $eobj->published) ? trim((string) $eobj->published) : 0));
              if (trim((string) $eobj->title)) $entry["title"] = trim((string) $eobj->title);
              if (trim((string) $eobj->link)) $entry["link"] = trim((string) $eobj->link);
              $entry["uniqueid"] = (!trim((string) $eobj->id) ? trim((string) $eobj->link) : trim((string) $eobj->id));

              if (trim((string) $eobj->author)) $entry["author"] = trim((string) $eobj->author);
              elseif (trim((string) $fobj->author)) $entry["author"] = trim((string) $fobj->author);

              $authurl = @$eobj->getAuthor (array ("param"=>"uri"));
              if ($authurl) $entry["author_url"] = $authurl;
              $authemail = @$eobj->getAuthor (array ("param"=>"email"));
              if ($authemail && $authemail != "noreply@blogger.com") $entry["author_email"] = $authemail;

              if (trim((string) $eobj->summary)
                && trim((string) $eobj->content)
                && trim((string) $eobj->summary) != trim((string) $eobj->content)) $entry["summary"] = trim((string) $eobj->summary);

              if (trim((string) $eobj->content) && trim((string) $eobj->content) != $entry["title"]) $entry["content"] = trim((string) $eobj->content);
              if (empty(trim((string) $eobj->content))
                && trim((string) $eobj->summary)
                && trim((string) $eobj->summary) != $entry["title"]) $entry["content"] = trim((string) $eobj->summary);


              include ("$BASE-fixes-entries.php");


              $entry["uniqueid"] = preg_replace ("/^https\:\/\//", "http://", $entry["uniqueid"]);


              if (skip ($feed, $entry)) {
                if (defined("debug") && constant ("debug")) {
                  error_log (
                    date ("Y-m-d")
                    . "\t{$feed["url"]}\tskipped\t"
                    . (!empty($entry["title"]) ? "{$entry["title"]}" : "")
                    . "\t" . (!empty($entry["link"]) ? "{$entry["link"]}" : "") . "\n"
                    , 3, "debug/{$BASE}_debug.log");
                }

              } else {
                if ((!isset ($entries[$entry["uniqueid"]]) || $entries[$entry["uniqueid"]]<$entry["updated"])) {

                  $bind = array();
                  if (!isset ($entries[$entry["uniqueid"]])) {
                    $qry = "INSERT IGNORE INTO {$BASE}_entry
                            SET
                              touch=NOW()
                              , created=NOW()
                              , groupid=:groupid
                              , feedid=:feedid";
                    $bind[":groupid"] = $feed["groupid"];
                    $bind[":feedid"] = $feed["feedid"];
                  } else {
                    $qry = "UPDATE {$BASE}_entry
                            SET touch=NOW()";
                  }
                  foreach ($entry as $field => $value) {
                    if (!empty ($value)) {
                      if (in_array ($field, array ("published", "updated"), true)) {
                        $qry .= ", $field=FROM_UNIXTIME(:$field)";
                      } else {
                        $qry .= ", $field=:$field";
                      }
                      $bind[":$field"] = $value;
                    }
                  }
                  if (isset ($entries[$entry["uniqueid"]])) {
                    $qry .= " WHERE
                                groupid=:groupid2
                                AND uniqueid=:uniqueid";
                    $bind[":groupid2"] = $feed["groupid"];
                    $bind[":uniqueid"] = $entry["uniqueid"];
                  }
                  $st = $DB["link"]->prepare ($qry);
                  foreach ($bind as $param => $value) {
                    $st->bindValue ($param, $value, PDO::PARAM_STR);
                  }
                  $st->execute();
                  $st->closeCursor ();


                  $uniqueid = $entry["uniqueid"];
                  $st2->execute();
                  $eid = $st2->fetchColumn ();
                  $st2->closeCursor ();


                  if (isset ($entries[$entry["uniqueid"]])) {
                    $st3->execute();
                    $st3->closeCursor ();
                  }


                  if (!isset ($entries[$entry["uniqueid"]]) && !empty($kindleits) && !empty($entry["link"])) {
                    foreach ($kindleits as $account => $dummy) {
                      set_time_limit (32);
                      $url = "http://fivefilters.org/kindle-it/send.php?context=send&url=" . rawurlencode ($entry["link"]);
                      curl_setopt_array ($curl, array (
                          CURLOPT_URL => $url,
                          CURLOPT_FAILONERROR => false,
                          CURLOPT_HEADER => false,
                          CURLOPT_HTTPHEADER => array(),
                          CURLOPT_POST => true,
                          CURLOPT_POSTFIELDS => "domain=1&email=" . rawurlencode ($account)
                        ));
                      if (!($kindleit=curl_exec($curl)) || curl_errno($curl)) {
                        error_log (date ("Y-m-d H:i:s") . "\tcurl exec #" . curl_errno ($curl) . " " . curl_error ($curl) . " of '$url'\n", 3, "$BASE.log");
                      } else {
                        if (!preg_match ("/\<div\s+id\s*\=\s*\"?success\"?\s*\>.+\<h3\>\s*Sent!\s*\<\/h3\>\s*\<p\>\s*Sent\s*to\s*\<strong\>/m", $kindleit)) {
                          error_log (date ("Y-m-d H:i:s") . "\tkindleit response '" . preg_replace ("/[\r\n\t ]+/", " ", $kindleit) . "'\n", 3, "$BASE.log");
                        }
                      }
                    }
                  }


                  if (!isset ($entries[$entry["uniqueid"]]) && !empty($emailtos)) {
                    foreach ($emailtos as $account => $email) {
                      $st4->bindValue (":account", $account, PDO::PARAM_STR);
                      $auth = substr (preg_replace ("/[^a-z0-9]/i", "", base64_encode (sha1 (rand ()))), 0, 8);
                      $st4->bindValue (":auth", $auth, PDO::PARAM_STR);
                      $st4->execute();
                      $st4->closeCursor ();


                      set_time_limit (32);
                      if (defined("debug") && constant ("debug")) {
                        error_log (date ("Y-m-d H:i:s") . "\ttrying to send mail to '$email' with the title '{$feed["title"]}'\n", 3, "$BASE.log");
                      }
                      if (!mail (
                        $email
                        , "=?utf-8?B?".base64_encode($feed["title"]." / ".$entry["title"].(!empty($entry["author"]) ? " - ".$entry["author"] : ""))."?="
                        , (empty ($conf["reader"]["state_read"]) ? ""
                          : "<img src=\"" 
                            . (!empty ($conf["base"]["url"]) ? $conf["base"]["url"] : "")
                            . "$BASE.php?account=" . rawurlencode ($account) . "&read=$eid&auth=" . rawurlencode ($auth) . "\"/>")
                        . (!empty($entry["content"]) ? $entry["content"] : "") . "\r\n"
                        . (empty ($entry["link"]) ? ""
                          : "\r\n<br/><br/><a href=\""
                            . (empty ($conf["reader"]["state_visited"]) ? $entry["link"]
                              : (!empty ($conf["base"]["url"]) ? $conf["base"]["url"] : "")
                                . "$BASE.php?account=" . rawurlencode ($account) . "&visit=$eid&auth=" . rawurlencode ($auth))
                            . "\">" . $entry["link"] . "</a>")
                        , "Content-Type: text/html; charset=utf-8\r\n"
                          . (!empty ($conf["email"]["from"]) ? "from: " . trim ($conf["email"]["from"]) . "\r\n" : ""))) {
                        error_log (date ("Y-m-d H:i:s") . "\tmail error '$email'\n", 3, "$BASE.log");

                      } else {
                        $st5->bindValue (":account", $account, PDO::PARAM_STR);
                        $st5->bindValue (":auth", $auth, PDO::PARAM_STR);
                        $st5->bindValue (":forwardedto", $email, PDO::PARAM_STR);
                        $st5->execute();
                        $st5->closeCursor ();
                      }
                    }
                  }


                  $entries[$entry["uniqueid"]] = $entry["updated"];


                  $waybackurls = array($feed["url"]);
                  if (!empty($feed["link"])) $waybackurls[] = $feed["link"];
                  foreach ($waybackurls as $url) {
                    if (!preg_match("/^https?:\/\/(moli\.hu|moli\.ddns\.net)\//", $url)) $wayback[$url] = true;
                  }
                }
              }


              $fobj->next ();
            }
          }
        } catch (XML_Feed_Parser_Exception $exc) {
          if ($exc->getMessage () == "Invalid input: this is not valid XML") {
            file_put_contents ("debug/" . preg_replace("/[^a-z0-9]/i","_",$feed["url"]).".".time().".txt", $response);
          }
          error_log (
            date("Y-m-d H:i:s")
            . "\tXML_Feed_Parser #".$exc->getCode()." ".$exc->getMessage()." @'{$feed["url"]}'\n"
            , 3, "$BASE.log");
        }
      }
    }


    $bind = array();
    $qry = "UPDATE {$BASE}_feed
            SET
              nextfetch=DATE_ADD(NOW(), INTERVAL fetchrate MINUTE)";
    foreach (array ("title", "link", "etag", "lastmod", "updated") as $field) {
      if (!empty ($feed[$field])) {
        if (in_array ($field, array("updated", "lastmod"), true)) {
          $qry .= ", $field=FROM_UNIXTIME(:$field)";
          $bind[":$field"] = (is_numeric ($feed[$field]) ? $feed[$field] : strtotime ($feed[$field]));
        } else {
          $qry .= ", $field=:$field";
          $bind[":$field"] = $feed[$field];
        }
      }
    }
    $qry .= " WHERE
                feedid=:feedid";
    $bind[":feedid"] = $feed["feedid"];
    $st = $DB["link"]->prepare ($qry);
    foreach ($bind as $param => $value) {
      $st->bindValue ($param, $value, PDO::PARAM_STR);
    }
    $st->execute();
    $st->closeCursor ();
  }
  $st0->closeCursor ();


  if (!empty($wayback)) file_put_contents("$BASE.wayback", "\n".implode("\n",array_keys($wayback))."\n", FILE_APPEND | LOCK_EX);
