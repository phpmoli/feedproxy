CREATE TABLE `feedproxy_feed` (
  `feedid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `groupid` varchar(63) DEFAULT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  `fetchrate` smallint(5) unsigned NOT NULL DEFAULT '15',
  `nextfetch` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `etag` varchar(255) DEFAULT NULL,
  `lastmod` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`feedid`),
  UNIQUE KEY `url` (`url`),
  KEY `nextfetch` (`nextfetch`)
) ENGINE=MyISAM AUTO_INCREMENT=464 DEFAULT CHARSET=utf8;


CREATE TABLE `feedproxy_subs` (
  `account` varchar(63) NOT NULL DEFAULT '',
  `feedid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `active` enum('yes') DEFAULT 'yes',
  `kindleit` enum('no','yes') NOT NULL DEFAULT 'no',
  `emailto` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `feedid_account` (`feedid`,`account`)
) ENGINE=MyISAM AUTO_INCREMENT=464 DEFAULT CHARSET=utf8;


CREATE TABLE `feedproxy_entry` (
  `entryid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `feedid` smallint(5) unsigned NOT NULL,
  `groupid` varchar(63) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uniqueid` blob,
  `link` text,
  `author` varchar(255) DEFAULT NULL,
  `author_url` varchar(255) DEFAULT NULL,
  `author_email` varchar(255) DEFAULT NULL,
  `title` text,
  `summary` text,
  `content` text,
  `touch` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`entryid`),
  UNIQUE KEY `groupid_uniqueid` (`groupid`,`uniqueid`(255)),
  KEY `feedid` (`feedid`)
) ENGINE=MyISAM AUTO_INCREMENT=375892 DEFAULT CHARSET=utf8;


CREATE TABLE `feedproxy_state` (
  `account` varchar(63) NOT NULL DEFAULT '',
  `entryid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `state` enum('new','forwarded','read','visited') NOT NULL DEFAULT 'new',
  `forwardedto` varchar(63) DEFAULT NULL,
  `forwardedtimes` smallint(5) unsigned NOT NULL DEFAULT '0',
  `auth` varchar(63) NOT NULL DEFAULT '',
  `ip` varchar(15) DEFAULT NULL,
  `touch` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `entryid_account` (`entryid`,`account`)
) ENGINE=MyISAM AUTO_INCREMENT=375892 DEFAULT CHARSET=utf8;
