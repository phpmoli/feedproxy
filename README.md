feedproxy
=========

**it's like a personal Google Reader server**



A backend to aggregate many rss or atom news feeds into one, your own feed.

You now subscribe to feeds not in your graphical feed reader client but in the feedproxy database. Feedproxy then fetches the feeds periodically and stores the news into it's database in the background. You just have to subscribe to a single one feed provided by feedproxy to get the previously stored news in your regular feed reader. It's transparent.

Helps the problem of reading the same news again and again on many computers you use - as those clients work individually, every news is new to them. Feedproxy remembers you've already read one and wont send it to you again.

Also useful when you go on vacation, as the proxy automatically fetches the content into database without the need to run a graphical client.

It's like a Google Reader alternative without the Evil.

Supports Amazon Kindle, can send the referred article contents with images automatically as a book via Kindle-It.


Features
* Intended Audience: Advanced End Users
* requires an always online server with `php`, `curl`, `pear`, `mysql`, `httpd`
* fetch frequency set individually by watched feeds
* supports multiple user accounts
* (optional) marks news read when your client showed the news to you
* (optional) marks news visited when you opened the news link in your client
* send news to your Kindle - automatic, not only summary, full article with images
* pseudo-feeds: a watched feed with a bogus url, so you can insert entries directly into the database (like email notifications)
* you can filter contents of any feed by defining rules (using php)
* can fix broken feeds you couldnt subscribe to in most readers
* stable code



Some tweaks:
* turn on read receipt in the configuration file `feedproxy.conf` by setting `[reader]/state_read` to `true`
* can measure which news link you visited by setting `[reader]/state_visited` to `true` in the configuration file `feedproxy.conf`
* if you are using various feed reader clients you can change the default configuration options in the query string
   e.g. `http://example.com/feedproxy/feedproxy.php?missing_http204=1`
* set up multiple accounts by specifing an account name for a feed in the `feedproxy_subs` table
     `UPDATE feedproxy_subs SET account='johndoe' WHERE account='' AND feedid=1;`
   now use the following url: `http://example.com/feedproxy/feedproxy.php?account=johndoe`
* send the content of selected feeds to your Kindle via Kindle-It from Five Filters
     `UPDATE feedproxy_subs SET kindleit='yes' WHERE account='johndoe' AND feedid=1;`
   make sure you name your account to the username of your kindle email address!
   e.g. if your kindle email is `janedoe@free.kindle.com` then feedproxy account name must be `janedoe`!
* send some of the subscription news by email to a specified email address
     `UPDATE feedproxy_subs SET emailto='send.email.here@example.com, and.an.other.copy.here@example.com' WHERE account='johndoe' AND feedid=1;`
* you can add an optional password so others knowing your account name cannot read your news
   create a file named `pw_account_johndoe_{$md5string}.php` where `'{$md5string}'` is a random md5 string
   now use the following url: `http://example.com/feedproxy/feedproxy.php?account=johndoe&auth=password`
   if the password file exists on the server, you must provide your password in every query
* test your installation by using `testfeed.php` as a feed provider url. add a new feed to the database
     `INSERT INTO feedproxy_feed SET url='http://example.com/feedproxy/testfeed.php'; INSERT INTO feedproxy_subs SET feedid=LAST_INSERT_ID();`
   it will create a news entry on every run, if you set the fetchrate to 15 minutes then every 15 minutes



INSTALLATION:
 1. copy feedproxy to an always online server
 2. install `php-cli`, `php-pear`, `php-curl`, `php-mysql` onto the server
 3. install `XML_Feed_Parser` PEAR package onto the server
 4. create a mysql access for feedproxy
 5. create feedproxy database tables by using the scheme in `feedproxy.sql`
 6. replace placeholder mysql hostname, username and database name in `feedproxy.conf` to your newly created access data
 7. replace placeholder mysql password in the 3rd line of `pw_db*.php` file
 8. install a crontab, see example file `feedproxy.crontab`
 9. insert a database row for a feed you'd like to read
    e.g. `INSERT INTO feedproxy_feed SET url='http://moli.hu/gamerankings/gamerankings.atom';`
    you can modify the default fetchrate (15 minutes) e.g. `UPDATE feedproxy_feed SET fetchrate=60 WHERE feedid=1;`
10. insert a database row for the feed you just inserted to subscribe to it
    e.g. `INSERT INTO feedproxy_subs SET feedid=1;`
11. add a new feed to your feed reader client, the address is your feedproxy installation url on your server
    e.g. `http://example.com/feedproxy/feedproxy.php`
